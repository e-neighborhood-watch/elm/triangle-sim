module Main
  exposing
    ( main
    )


import Array
  exposing
    ( Array
    )
import Browser
import Browser.Events
import Html
  exposing
    ( Html
    )
import Html.Lazy as Html
import Random
  exposing
    ( Generator
    )
import Svg
  exposing
    ( Svg
    )
import Svg.Attributes
  as SvgAttr
import Task
import Time


-- How can a configuration go wrong?
--  1. Too many faces on an edge
--  2. A person needs to be in two places at once.


-- p1 < p2
type alias Actor =
  { physics : Physics
  , p1 : Int
  , p2 : Int
  }


type alias Physics =
  { x : Float
  , y : Float
  , vx : Float
  , vy : Float
  }


type alias Model =
  { actors : Maybe (Array Actor)
  , actorViewLocs : List (Float, Float)
  , lastTime : Maybe Int
  , visible : Browser.Events.Visibility
  }


type Message
  = InitialTime Int
  | ActorsGenerated (Array Actor)
  | PhysicsTick Int
  | AnimationTick
  | VisibilityChanged Browser.Events.Visibility


init : {} -> ( Model, Cmd Message )
init {} =
  ( { actors =
      Nothing
    , actorViewLocs =
      []
    , lastTime =
      Nothing
    , visible =
      Browser.Events.Visible
    }
  , Cmd.batch
    [ Task.perform (Time.posixToMillis >> InitialTime) Time.now
    , Random.generate ActorsGenerated (randomActors 0.1 4)
    ]
  )


randomActors : Float -> Int -> Generator (Array Actor)
randomActors r n =
  let
    buildActor : Int -> (Int, Int) -> Actor
    buildActor ix (dp1,dp2) =
      let
        rot : Float
        rot =
          2 * pi * toFloat ix / toFloat n

        p1 : Int
        p1 =
          ix + dp1
            |> modBy n

        p2 : Int
        p2 =
          if
            dp1 + dp2 >= n
          then
            ix + dp1 + dp2 + 1
              |> modBy n
          else
            ix + dp1 + dp2
              |> modBy n
      in
        { physics =
          { x =
            r * cos rot
          , y =
            r * sin rot
          , vx =
            0
          , vy =
            0
          }
        , p1 =
          min p1 p2
        , p2 =
          max p1 p2
        }
  in
    Random.list n (Random.pair (Random.int 1 (n - 1)) (Random.int 1 (n - 2)))
      |> Random.map (List.map2 buildActor (List.range 0 (n - 1)) >> Array.fromList)


maxV : Float
maxV =
  1/32768


-- weight for moving away from too close actors
actorStiffness : Float
actorStiffness =
  0.5


-- minimum distance before starting to avoid actor
actorMinDistance : Float
actorMinDistance =
  0.08


-- weight for moving away from too close walls
wallStiffness : Float
wallStiffness =
  0.5


-- minimum distance before starting to avoid the wall
wallMinDistance : Float
wallMinDistance =
  0.05


-- weight for moving towards the nearest destination point
determination : Float
determination =
  1 / 2048


dist : Float -> Float -> Float -> Float -> Float
dist x1 y1 x2 y2 =
  (x1 - x2)^2 + (y1 - y2)^2
    |> sqrt


actorStep : Physics -> Array Physics -> Physics -> Physics -> Physics
actorStep physics otherPhysics target1Physics target2Physics =
  let
    altitudeX : Float
    altitudeX =
      (target1Physics.y - target2Physics.y) * sqrt 3 / 2

    altitudeY : Float
    altitudeY =
      (target2Physics.x - target1Physics.x) * sqrt 3 / 2

    midpointX : Float
    midpointX =
      (target1Physics.x + target2Physics.x) / 2

    midpointY : Float
    midpointY =
      (target1Physics.y + target2Physics.y) / 2

    dest1X : Float
    dest1X =
      midpointX + altitudeX

    dest1Y : Float
    dest1Y =
      midpointY + altitudeY

    dest2X : Float
    dest2X =
      midpointX - altitudeX

    dest2Y : Float
    dest2Y =
      midpointY - altitudeY

    dest1Dist : Float
    dest1Dist =
      dist physics.x physics.y dest1X dest1Y

    dest2Dist : Float
    dest2Dist =
      dist physics.x physics.y dest2X dest2Y

    wallEffectX : Float
    wallEffectX =
      max 0 (wallMinDistance - 1 - physics.x) + min 0 (1 - wallMinDistance - physics.x)

    wallEffectY : Float
    wallEffectY =
      max 0 (wallMinDistance - 1 - physics.y) + min 0 (1 - wallMinDistance - physics.y)

    (nearActorEffectX, nearActorEffectY) =
      Array.foldl
        ( \ other (effectX, effectY) ->
          let
            distance : Float
            distance =
              dist physics.x physics.y other.x other.y

            displacement : Float
            displacement =
              actorMinDistance - distance
                |> max 0
          in
            ( effectX + (physics.x - other.x)/distance*displacement
            , effectY + (physics.y - other.y)/distance*displacement
            )
        )
        (0,0)
        otherPhysics

    (destEffectX,destEffectY) =
      if
        dest1Dist <= dest2Dist
      then
        (dest1X - physics.x, dest1Y - physics.y)
      else
        (dest2X - physics.x, dest2Y - physics.y)

    totalEffectX : Float
    totalEffectX =
      wallStiffness*wallEffectX + actorStiffness*nearActorEffectX + determination*destEffectX

    totalEffectY : Float
    totalEffectY =
      wallStiffness*wallEffectY + actorStiffness*nearActorEffectY + determination*destEffectY

    totalEffectMagnitude : Float
    totalEffectMagnitude =
      dist totalEffectX totalEffectY 0 0
  in
    if
      totalEffectMagnitude < 2 ^ -25
    then
      { x =
        physics.x
      , y =
        physics.y
      , vx =
        0
      , vy =
        0
      }
    else
      let
        effectMagnitudeScaling : Float
        effectMagnitudeScaling =
          min 1 (maxV / totalEffectMagnitude)
      in
        { x =
          physics.x
        , y =
          physics.y
        , vx =
          totalEffectX*effectMagnitudeScaling
        , vy =
          totalEffectY*effectMagnitudeScaling
        }


physicsStep : Array Actor -> Actor -> Actor
physicsStep actors actor =
  case
    Maybe.map2
      ( actors
        |> Array.filter ((/=) actor)
        |> Array.map .physics
        |> actorStep actor.physics
      )
      ( Array.get actor.p1 actors
        |> Maybe.map .physics
      )
      ( Array.get actor.p2 actors
        |> Maybe.map .physics
      )
  of
    Nothing ->
      actor -- should never happen

    Just newPhysics ->
      { physics =
        newPhysics
      , p1 =
        actor.p1
      , p2 =
        actor.p2
      }


update : Message -> Model -> (Model, Cmd Message)
update msg prev =
    case
      msg
    of
      VisibilityChanged newVisibility ->
        ( { lastTime =
            Nothing
          , actors =
            prev.actors
          , actorViewLocs =
            prev.actorViewLocs
          , visible =
            newVisibility
          }
        , Task.perform (Time.posixToMillis >> InitialTime) Time.now
        )

      InitialTime time ->
        case
          prev.lastTime
        of
          Nothing ->
            ( { lastTime =
                Just time
              , actors =
                prev.actors
              , actorViewLocs =
                prev.actorViewLocs
              , visible =
                prev.visible
              }
            , Cmd.none
            )

          Just _ ->
            ( prev
            , Cmd.none
            )

      ActorsGenerated actors ->
        case
          prev.actors
        of
          Nothing ->
            ( { lastTime =
                prev.lastTime
              , actors =
                Just actors
              , actorViewLocs =
                prev.actorViewLocs
              , visible =
                prev.visible
              }
            , Cmd.none
            )

          Just _ ->
            ( prev
            , Cmd.none
            )

      AnimationTick ->
        case
          prev.actors
        of
          Nothing ->
            ( prev
            , Cmd.none
            )

          Just actors ->
            ( { lastTime =
                prev.lastTime
              , actors =
                prev.actors
              , actorViewLocs =
                Array.foldl ( \ { physics } -> (::) (physics.x,physics.y) ) [] actors
              , visible =
                prev.visible
              }
            , Cmd.none
            )

      PhysicsTick newTime ->
        case
          prev.actors
        of
          Nothing ->
            ( prev
            , Cmd.none
            )

          Just actors ->
            case
              prev.lastTime
            of
              Nothing ->
                ( prev
                , Cmd.none
                )

              Just lastTime ->
                let
                  elapsed : Float
                  elapsed =
                    newTime - lastTime
                      |> toFloat

                  newPosActors : Array Actor
                  newPosActors =
                    Array.map
                      ( \ actor ->
                        { physics =
                          { x =
                            actor.physics.x + elapsed*actor.physics.vx
                          , y =
                            actor.physics.y + elapsed*actor.physics.vy
                          , vx =
                            actor.physics.vx
                          , vy =
                            actor.physics.vy
                          }
                        , p1 =
                          actor.p1
                        , p2 =
                          actor.p2
                        }
                      )
                      actors
                in
                  ( { lastTime =
                      Just newTime
                    , actors =
                      Array.map (physicsStep newPosActors) newPosActors
                        |> Just
                    , actorViewLocs =
                      prev.actorViewLocs
                    , visible =
                      prev.visible
                    }
                  , Cmd.none
                  )


drawActor : (Float, Float) -> Svg a
drawActor (x, y) =
  Svg.circle
    [ SvgAttr.cx (String.fromFloat x)
    , SvgAttr.cy (String.fromFloat y)
    , SvgAttr.r "0.02"
    ]
    []


viewWorld : List (Float, Float) -> Html a
viewWorld =
  List.map drawActor
    >>
      (::)
        ( Svg.rect
          [ SvgAttr.x "-1"
          , SvgAttr.y "-1"
          , SvgAttr.width "2"
          , SvgAttr.height "2"
          , SvgAttr.fill "gray"
          ]
          []
        )
    >>
      Svg.svg
        [ SvgAttr.viewBox "-1 -1 2 2"
        , SvgAttr.preserveAspectRatio "xMidYMid"
        , SvgAttr.style "position: absolute; top: 0; left: 0; width: 100%; height: 100%"
        ]


view : Model -> Browser.Document Message
view { actorViewLocs } =
  { title =
    "Triangle Simulation"
  , body =
    [ Html.lazy viewWorld actorViewLocs
    ]
  }


subscriptions : Model -> Sub Message
subscriptions { lastTime, actors, visible } =
  case
    lastTime
  of
    Nothing ->
      Browser.Events.onVisibilityChange VisibilityChanged

    Just _ ->
      case
        actors
      of
        Nothing ->
          Browser.Events.onVisibilityChange VisibilityChanged

        Just _ ->
          case
            visible
          of
            Browser.Events.Hidden ->
              Browser.Events.onVisibilityChange VisibilityChanged

            Browser.Events.Visible ->
              Sub.batch
                [ Time.every (1000/100) (Time.posixToMillis >> PhysicsTick)
                , Browser.Events.onAnimationFrame (always AnimationTick)
                , Browser.Events.onVisibilityChange VisibilityChanged
                ]


main : Program {} Model Message
main =
  Browser.document
    { init =
      init
    , update =
      update
    , view =
      view
    , subscriptions =
      subscriptions
    }
